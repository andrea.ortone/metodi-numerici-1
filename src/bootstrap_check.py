#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
from glob import glob
from tqdm import tqdm
import multiprocessing


def chi_var(file):
    return (np.abs(np.loadtxt(file, unpack=True, usecols=(0)))).var(ddof=1)


if __name__ == '__main__':

    files = glob('build/boot/*.txt')

    with multiprocessing.Pool(processes=None) as p:
        chis = p.map(chi_var, files)

    chis = np.array(chis)

    print(f'chi = {30*30*0.42*chis.mean():.4f} pm {30*30*0.42*chis.std(ddof=1):.4f}')
    print(f'chi_boot = 16.5493 pm 0.0789')

    plt.plot(range(len(files)), chis, linestyle = '', marker = '.')
    plt.show()

    plt.hist(chis, density = True)
    X = np.linspace(min(chis), max(chis), 1000)
    Y = np.exp(-(X-chis.mean())**2/(2*chis.var(ddof=1)))/np.sqrt(2*np.pi*chis.var(ddof=1))
    plt.plot(X,Y)
    plt.show()

    # data = np.random.normal(0,1,200)
    # plt.hist(data, bins = 10)
    # plt.show()
