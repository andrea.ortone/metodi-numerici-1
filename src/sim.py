#!/usr/bin/env python3

import numpy as np
from subprocess import run
import threading, queue
import os
import time

Ls = [15,20,25,30,35,40,45,50,55,60]

q = queue.Queue()

def worker():
    while True:
        item = q.get()
        with open('config.yaml', 'w') as f:
            f.write(item)
        print('========================')
        print(item)
        print('========================')
        run('./ising')
        q.task_done()

for L in Ls:
        out = f'''
path:
  # non ci va il .txt
  out: mc

phys:
  beta:
    start: 0.38
    stop:  0.46
    steps: 70
  L: {L}

sim:
  iterazioni: {int(2.5e6*L*L)}

  # ogni quanti update misurare
  saveStep:   {20*L*L}
'''
        q.put(out)

for i in range(10):
    threading.Thread(target=worker, daemon=True).start()
    time.sleep(2)

q.join()
