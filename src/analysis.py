#!/usr/bin/env python3

import numpy as np
from utils import (
    load_data,
    bootstrap,
    blocking,
    choose_blockSize_bootstrap,
    choose_blockSize_blocking,
    get_Ls,
    blocking
)
from tqdm import tqdm
from sys import argv
import multiprocessing
import matplotlib.pyplot as plt


plt.style.use('style.yml')


def sample_variance_abs(data):
    return abs(data).std(ddof=1)**2


def bootstrap_parallel(N, beta, m, blockSize, func, M):
    return N*beta*bootstrap(m, blockSize, func, M).std(ddof=1)


if __name__=="__main__":
    path = argv[1]
    Ls = get_Ls(path)

    blockSizes = len(Ls)*[2000]

    betas_max = []
    chis_max = []
    chis_err_max = []

    outFile = open('chis.txt', 'w')

    for j, L in tqdm(enumerate(Ls), total=len(Ls), desc='Importing data: '):
        N, betas, ms, es = load_data(L, path=path, neglect_first=0, show=False)

        ### TERMALIZZAZIONE
        # for b in [45, -1]:
        #     plt.plot(range(len(ms[b])), ms[b])
        #     plt.title(f'L = {L}, beta = {betas[b]}')
        #     plt.xlim(0, 1000)
        #     plt.xlabel('$ T_{\mathrm{mc}} $')
        #     plt.ylabel('$ m $')
        #     plt.savefig(f'term/{L}-{betas[b]}.pdf', dpi=600)
        #     # plt.show()
        #     plt.close()

        ### CALCOLO DI m
        # choose_blockSize_blocking('M', L, ms[45], 1, 6000, 200)
        # plt.figure(1)
        # plt.errorbar(betas, [abs(m).mean() for m in ms], yerr=[blocking(abs(m), 2000) for m in ms], label=L)

        ### CALCOLO DI E
        # choose_blockSize_blocking('E', L, es[45], 1, 10000, 500)
        # plt.figure(2)
        # plt.errorbar(betas, [abs(e).mean() for e in ms], yerr=[blocking(e, 2000) for e in ms], label=L)

        ### CALCOLO DI \chi
        ## valori centrali
        chis = [
            N * beta * sample_variance_abs(ms[i])
            for i, beta in enumerate(betas)
        ]


        ## scelta della dimensione del blocco
        # choose_blockSize_bootstrap(L, abs(ms[40]), sample_variance_abs, 1, 10000, 100)
        # continue

        ## calcolo gli errori
        with multiprocessing.Pool(processes=None) as p:
            chi_errs = p.starmap(
                bootstrap_parallel,
                [
                    (N, betas[i], ms[i], blockSizes[j], sample_variance_abs, 1000)
                    for i in range(len(betas))
                ]
            )

        # scrivo le cose
        for i in range(len(betas)):
            outFile.write(f'{L} {betas[i]} {chis[i]} {chi_errs[i]}\n')

        ## trovo il massimo
        arg = np.argmax(chis)
        betas_max.append(betas[arg])
        chis_max.append(chis[arg])
        chis_err_max.append(chi_errs[arg])

    # plt.figure(1)
    # plt.legend()
    # plt.xlabel(r'$ \beta $')
    # plt.ylabel('$ |m| $')

    # plt.figure(2)
    # plt.legend()
    # plt.xlabel(r'$ \beta $')
    # plt.ylabel('$ E $')

    # plt.savefig('../doc/fig/m-beta.pdf', dpi=600)
    # plt.show()

    outFile.close()

    with open("maxima.txt", "w") as outFile:
        for i in range(len(Ls)):
            outFile.write(f'{Ls[i]} {betas_max[i]} {chis_max[i]} {chis_err_max[i]}\n')
