#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

Ls, chis_max, chi_errs_max = np.loadtxt('maxima.txt', unpack=True, usecols=(0, 2, 3))


def fit(x, A, B):
    return A*x + B


froms = 4

for frm in range(froms):
    to = frm + len(Ls) - froms + 1
    _Ls = Ls[frm:to]
    _chis_max = chis_max[frm:to]
    _chi_errs_max = chi_errs_max[frm:to]


    opt, cov = curve_fit(
        fit, np.log(_Ls), np.log(_chis_max),
        sigma=_chi_errs_max/_chis_max, absolute_sigma=True
    )

    ndof = len(_Ls) - 2
    chi2 = (((np.log(_chis_max) - fit(np.log(_Ls), *opt))/(_chi_errs_max/_chis_max))**2).sum()

    print(f'{frm}-{to-1}: γ/ν = {opt[0]:.5f} ± {np.sqrt(cov[0][0]):.5f}\t\tdist = {(abs(opt[0])-7/4)/(np.sqrt(cov[0][0])):.2f}\tchi2 = {chi2}\tndof = {ndof}')
    # print(f'γ/ν_exp = {7/4}')

    X = np.linspace(min(np.log(_Ls)), max(np.log(_Ls)), 1000)
    plt.errorbar(np.log(_Ls), np.log(_chis_max), yerr=_chi_errs_max/_chis_max, marker='x', linestyle='')
    plt.plot(X, fit(X, *opt))
    plt.show()
