
#include <iostream>
#include <string>
#include <random>
#include <time.h>
#include <boost/dynamic_bitset.hpp>
#include <fstream>
#include <yaml-cpp/yaml.h>

using namespace std;


int main()
{
    // Leggo i parametri
    YAML::Node config = YAML::LoadFile("config.yaml");

    string   outPath   = config["path"]["out"].as<string>();

    double   betaStart = config["phys"]["beta"]["start"].as<double>();
    double   betaStop  = config["phys"]["beta"]["stop"].as<double>();
    unsigned betaSteps = config["phys"]["beta"]["steps"].as<unsigned>();
    unsigned L         = config["phys"]["L"].as<unsigned>();

    unsigned long long iterazioni = config["sim"]["iterazioni"].as<unsigned long long>(); // non sweep
    unsigned saveStep   = config["sim"]["saveStep"].as<unsigned>(); // ogni quanto misurare

    const unsigned N = L*L;
    boost::dynamic_bitset <> S(N, 0);

    // Calcolo chi sono i primi vicini
    unsigned nn_r[N];
    unsigned nn_l[N];
    unsigned nn_u[N];
    unsigned nn_d[N];

    for(unsigned p=0; p<N; p++)
        nn_r[p] = (p+1)%L + L*(p/L);
    for(unsigned p=0; p<N; p++)
        nn_l[p] = (p-1+L)%L + L*(p/L);
    for(unsigned p=0; p<N; p++)
        nn_u[p] = (p-L+N)%N;
    for(unsigned p=0; p<N; p++)
        nn_d[p] = (p+L)%N;

    mt19937 gen(time(0));
    uniform_int_distribution<unsigned> dis(0, N-1);
    uniform_real_distribution<double>  dis01(0,1);

    // Inizializzo il reticolo metà 0 e metà 1
    for (unsigned i=0; i<N; i++)
        if(dis01(gen) > 0.5)
            S.set(i);

    double beta;
    int energy = 0;
    int deltaE;
    unsigned p;
    unsigned acc;

    for (unsigned i=0; i<N; i++)
        energy -= 2*(2*S[i]-1)*(S[nn_r[i]] + S[nn_d[i]] - 1);

    for (unsigned i=0; i<betaSteps; i++)
    {
        beta = betaStart + i*(betaStop - betaStart)/betaSteps;
        ofstream outFile(outPath + "-" + to_string(L) + "-" + to_string(beta) + ".txt");
        cout << "Inizio la simulazione per beta = " << beta;
        acc = 0;

        for (unsigned long long step=0; step<iterazioni; step++)
        {
            p = dis(gen);
            deltaE = 4*(2*S[p] - 1)*(S[nn_r[p]] + S[nn_l[p]] + S[nn_d[p]] + S[nn_u[p]] - 2);

            if (deltaE <= 0 || (dis01(gen) < exp(-beta*deltaE)))
            {
                S.flip(p);
                energy += deltaE;
                acc++;
            }

            if (!(step%saveStep))
                outFile << (2.*S.count()/N - 1) << " " << ((float)energy)/N << "\n";
        }

        cout << "\taccettanza = " << ((double)acc)/iterazioni << endl;

        outFile.close();
    }

    return 0;
}
