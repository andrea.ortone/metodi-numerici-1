#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit


def fit(x, A, B, C):
    return A + B*(x**(-1/C))


plt.style.use('style.yml')

Ls, betas_max = np.loadtxt('maxima.txt', unpack=True, usecols=(0, 1))

beta_err = 0.001143
T_err = beta_err/(betas_max**2)

opt, cov = curve_fit(fit, Ls, 1/betas_max, sigma=T_err, absolute_sigma=True)

print(f'ν = {opt[2]:.3f} ± {np.sqrt(cov[2][2]):.3f}')
print(f'ν_exp = 1')
print(f'T_c = {opt[0]:.3f} ± {np.sqrt(cov[0][0]):.3f}')
print(f'T_c_exp = {2/np.log(1+np.sqrt(2)):.3f}')

plt.errorbar(Ls, 1/betas_max, T_err, marker='x')
X = np.linspace(min(Ls), max(Ls), 1000)
plt.plot(X, fit(X, *opt))
plt.xlabel('$ L $')
plt.ylabel(r'$ \bar T(L) $')
plt.savefig('betac.pdf')
plt.show()
