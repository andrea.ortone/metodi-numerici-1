import numpy as np
from glob import glob
from re import search
import matplotlib.pyplot as plt
import multiprocessing


def get_Ls(path='build'):
    files = sorted(glob(path + '/mc*.txt'))
    Ls = []

    for f in files:
        match = search('-(.*?)-(.*?)\.txt', f)
        L = int(match.group(1))
        if L not in Ls:
            Ls.append(L)

    return sorted(Ls)


def import_file(file):
    return np.loadtxt(
        file, unpack=True,
        dtype={'names': ('m', 'E'), 'formats': ('f', 'f')}
    )


def load_data(Ltgt, path='build', show=False, neglect_first=0):
    files = sorted(glob(path + f'/mc-{Ltgt}-*.txt'))

    with multiprocessing.Pool(processes=None) as p:
        imported_data = p.map(import_file, files)

    imported_data = np.array(imported_data)

    betas = []

    for f in files:
        match = search('-(.*?)-(.*?)\.txt', f)
        betas.append(float(match.group(2)))

    return Ltgt**2, betas, imported_data[:,0,:], imported_data[:,1,:]


def blocking(data, blockSize):
    Nblocks = int(len(data)/blockSize)

    blocks = [
        data[i*blockSize: (i+1)*blockSize]
        for i in range(Nblocks)
    ]

    avgs = np.array([
        b.mean()
        for b in blocks
    ])

    return avgs.std(ddof=1)/np.sqrt(Nblocks) if Nblocks > 1 else 0


# à la D'Elia
def bootstrap(data, blockSize, func, M=1000):
    Nblocks = int(len(data)/blockSize)
    blocks = [
        data[i*blockSize: (i+1)*blockSize]
        for i in range(Nblocks)
    ]

    to_return = np.full(M, 0.)

    tmp = np.full((Nblocks, blockSize), 0.)
    for j in range(M):
        for k, b in enumerate(np.random.randint(0, Nblocks, size=Nblocks)):
            tmp[k] = blocks[b]
        to_return[j] = func(tmp.flatten())

    return to_return


def choose_blockSize_blocking(label, L, max_corr_data, min=1, max=50, step=5):
    stds = []
    blockSizes = range(min, max, step)

    for blockSize in blockSizes:
        stds.append(
            blocking(max_corr_data, blockSize)
        )

    plt.plot(blockSizes, stds, label=L)
    # plt.show()
    plt.xlabel('block size')
    plt.ylabel(r'$ \sigma_m $')
    plt.legend(loc='lower right')
    plt.savefig(f'block/parolina-{label}-{L}.pdf', dpi=600)

def choose_blockSize_bootstrap(L, max_corr_data, func, min=1, max=50, step=5):
    stds = []
    blockSizes = range(min, max, step)

    for blockSize in blockSizes:
        stds.append(
            (bootstrap(max_corr_data, blockSize, func)).std(ddof=1)
        )

    plt.plot(blockSizes, stds, label=L)
    plt.xlabel('block size')
    plt.ylabel(r'$ \sigma_\chi $')
    plt.legend(loc='lower right')
    plt.savefig(f'block_chi/{L}.pdf', dpi=600)
