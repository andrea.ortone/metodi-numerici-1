#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
from sys import argv
from tqdm import tqdm
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from scipy.optimize import minimize
import multiprocessing


plt.style.use('style.yml')

betac = np.log(1+np.sqrt(2))/2
nu_v = 1
gamma_v = 7/4


def cov2corr(cov: np.ndarray) -> np.ndarray:
    std_err = np.sqrt(np.diag(cov))
    return cov / np.outer(std_err, std_err)

def beta_x (x, L, nu, betac):
    return 1/( x*(L**(-1/nu)) + 1/betac )


def x_beta (beta, L, nu, betac):
    return (1/beta - 1/betac) * (L**(1/nu))


def chi(L: int, beta_y, chis_L, chi_errs_L, betas_L):      # chis_L e betas_L sono 1d-array
    chi = []
    err_chi = []
    for i in range(len(beta_y)):
        index = (betas_L < beta_y[i]).sum() - 1  # indice del betas_L più vicino a sinistra
        m = (chis_L[index+1]-chis_L[index])/(betas_L[index+1]-betas_L[index])
        chi.append(chis_L[index] + m * (beta_y[i]-betas_L[index]))
        err_chi.append(
            np.sqrt(
                ((betas_L[index+1]-beta_y[i])*chi_errs_L[index])**2 +
                ((betas_L[index]-beta_y[i])*chi_errs_L[index+1])**2
            )/(betas_L[index+1]-betas_L[index])
        )
    return np.array(chi), np.array(err_chi)


def F(betac, nu, gamma, chis, betas, chi_errs, Ls, n):
    # assumo beta ordinati in modo crescente e della stessa lunghezza per i vari L


    # plt.title(f'FSS {nu:.2f} - {gamma:.2f}')
    # plt.xlabel(r'$(T-T_c)L^{1/\nu}$')
    # plt.ylabel(r'$\chi L^{-\gamma/\nu}$')
    # plt.ylim(0,0.05)
    # plt.legend()
    # plt.show()


    x_maxs = x_beta(betas[:, 1], Ls, nu, betac)         # 0 al posto di 1
    x_mins = x_beta(betas[:, -2], Ls, nu, betac)        # -1 al posto di -2


    # couples = [(i, j) for i in range(len(Ls)) for j in range(i)]
    couples = [(i, i+1) for i in range(len(Ls)-1)]

    _sum = 0

    for (i,j) in couples:
        x_min_i = x_mins[i]
        x_min_j = x_mins[j]
        x_max_i = x_maxs[i]
        x_max_j = x_maxs[j]

        min_x = max(x_min_i, x_min_j)
        max_x = min(x_max_i, x_max_j)

        if min_x > max_x:
            raise Exception

        y = np.linspace(min_x, max_x, n)
        beta_y_i = beta_x(y, Ls[i], nu, betac)
        beta_y_j = beta_x(y, Ls[j], nu, betac)

        g_i, errg_i = chi(Ls[i], beta_y_i, chis[i], chi_errs[i], betas[i])
        g_j, errg_j = chi(Ls[j], beta_y_j, chis[j], chi_errs[j], betas[j])
        g_i *= Ls[i]**(-gamma/nu)
        g_j *= Ls[j]**(-gamma/nu)
        errg_i *= Ls[i]**(-gamma/nu)
        errg_j *= Ls[j]**(-gamma/nu)

        err_g = np.sqrt(errg_i**2 + errg_j**2)

        # plt.plot(beta_y_i, g_i)
        # plt.plot(beta_y_j, g_j)
        # plt.show()

        # plt.title(f'{Ls[i]}, {Ls[j]}')
        # plt.xlim(min(x_mins), max(x_maxs))
        # plt.ylim(0,0.05)
        # plt.plot(y, g_i)
        # plt.plot(y, g_j)
        # plt.show()


        _sum += (np.abs(g_i-g_j)/err_g).sum()
    return _sum/n


# trovo il minimo
def find_F_minimum(chis, betas, chi_errs, Ls):
    nu, gamma = minimize(
        lambda x: F(betac, x[0], x[1], chis, betas, chi_errs, Ls, 20),
        (nu_v, gamma_v),
        method='Nelder-Mead',
        # bounds = ([0.8,1.2], [1.5,2.]),
        options={
            # 'maxiter': 1000,
            'disp': True
        }
    ).x

    # for i in range(len(Ls)):
    #     x = (1/np.array(betas[i]) - 1/betac)*Ls[i]**(1/nu)
    #     y = chis[i]*Ls[i]**(-gamma/nu)
    #     plt.errorbar(
    #         x, y, yerr=chi_errs[i]*Ls[i]**(-gamma/nu),
    #         marker='.', linestyle='', label=Ls[i]
    #     )

    # plt.xlabel(r'$(T-T_c)L^{1/\nu}$')
    # plt.ylabel(r'$\chi L^{-\gamma/\nu}$')
    # plt.legend()
    # plt.show()

    return (nu, gamma)


if __name__ == '__main__':
    # Carico i dati
    _Ls, _betas, _chis, _chi_errs = np.loadtxt(
        argv[1], unpack=True,
        dtype={
            'names': ('Ls', 'betas', 'chis', 'chi_errs'),
            'formats': ('i', 'f', 'f', 'f')
        }
    )

    # Divido i dati in base a L
    occ = [0]
    c = 0
    tmp = _Ls[0]

    for L in _Ls:
        if L != tmp:
            occ.append(c)
            tmp = L
        c += 1

    occ.append(len(_Ls))

    betas, chis, chi_errs = map(
        lambda x: [
            x[occ[i]:occ[i+1]]
        for i in range(len(occ)-1)
        ],
        [_betas, _chis, _chi_errs]
    )
    betas = np.array(betas)
    chis = np.array(chis)
    chi_errs = np.array(chi_errs)
    Ls = np.unique(_Ls)

    # Plot della \chi
    for i in range(len(Ls)):
        plt.errorbar(
            betas[i], chis[i], yerr=chi_errs[i],
            marker='.', linestyle='', label=Ls[i],
            markersize=1
        )

    plt.plot(2*[np.log(1+np.sqrt(2))/2], [0, 70])
    plt.xlabel(r'$\beta$')
    plt.ylabel(r'$\chi$')
    plt.legend()
    plt.savefig('chis.pdf')
    plt.show()

    # FSS assumendo parametri
    # for i in range(len(Ls)):
    #     x = (1/np.array(betas[i]) - 1/betac)*Ls[i]**(1/nu_v)
    #     y = chis[i]*Ls[i]**(-gamma_v/nu_v)
    #     plt.errorbar(
    #         x, y, yerr=chi_errs[i]*Ls[i]**(-gamma_v/nu_v),
    #         marker='x', linestyle='', label=Ls[i]
    #     )

    # plt.title(r'FSS assumendo $ \beta_c $, $ \gamma $, $ \nu $')
    # plt.xlabel(r'$(T-T_c)L^{1/\nu}$')
    # plt.ylabel(r'$\chi L^{-\gamma/\nu}$')
    # plt.legend()
    # plt.show()

    # FSS con minimizzazione
    k = 50  # per la meshgrid
    n = 20  # risoluzione della distanza tra funzioni
    nus = np.linspace(0.7,1.1, k)
    gammas = np.linspace(1.5,2, k)

    frm = 6
    to = 10

    # NU
    Fs_n = []
    for nu in nus:
        Fs_n.append(F(betac, nu, gamma_v, chis[frm:to], betas[frm:to], chi_errs[frm:to], Ls[frm:to], n))

    Fs_n = np.array(Fs_n)
    plt.plot(nus, Fs_n)
    plt.title(r'$ \gamma = 7/4 $')
    plt.xlabel(r'$ \nu $')
    plt.ylabel('$ F $')
    plt.savefig('F-noto-gamma.pdf')
    plt.show()


    # GAMMA
    Fs_g = []
    for gamma in gammas:
        Fs_g.append(F(betac, nu_v, gamma, chis[frm:to], betas[frm:to], chi_errs[frm:to], Ls[frm:to], n))

    Fs_g = np.array(Fs_g)
    plt.plot(gammas, Fs_g)
    plt.title(r'$ \nu = 1 $')
    plt.xlabel(r'$ \gamma $')
    plt.ylabel('$ F $')
    plt.savefig('F-noto-nu.pdf')
    plt.show()

    # GAMMA NU
    z = np.full((len(nus), len(gammas)), 0.)
    for i, _nu in tqdm(enumerate(nus), total = len(nus)):
        for j, g in enumerate(gammas):
            z[j,i] = F(betac, _nu, g, chis[frm:to], betas[frm:to], chi_errs[frm:to], Ls[frm:to], n)

    g_min, n_min = np.unravel_index(np.argmin(z, axis=None), z.shape)
    print(f'nu = {nus[n_min]}, gamma = {gammas[g_min]}')

    # 3D plot
    fig = plt.figure()
    ax = Axes3D(fig)
    B, D = np.meshgrid(nus, gammas)
    ax.plot_surface(B, D, z, cmap=cm.coolwarm, alpha=0.6)
    ax.plot(nus, [gamma_v]*k, np.array(Fs_n), color='red')
    ax.plot([nu_v]*k, gammas, np.array(Fs_g), color='blue')

    plt.xlabel(r'$ \nu $')
    plt.ylabel(r'$ \gamma $')
    plt.savefig('F-gamma-nu.pdf')
    plt.show()

    # Contour Plot
    fig = plt.figure()
    ax = fig.add_subplot(1,1,1)
    C = ax.contourf(B, D, z, 100)
    CB = plt.colorbar(C)
    plt.plot(nu_v, gamma_v, 'rx')  # vero

    # plt.xlim(0.9, 1.15)
    # plt.ylim(1.64, 1.80)
    plt.xlabel(r'$ \nu $')
    plt.ylabel(r'$ \gamma $')
    plt.savefig('F-gamma-nu-contour.pdf')
    plt.show()

    # stima degli esponenti critici via minimizzazione

    for mm in tqdm(range(0, 7)):
        # modo 1: escludo via via i dati con L minore
        # _chis = chis[mm:]
        # _chi_errs = chi_errs[mm:]
        # _Ls = Ls[mm:]
        # _betas = betas[mm:]

        # modo 2: prendo 4 dati per volta
        _chis = chis[mm:mm+4]
        _chi_errs = chi_errs[mm:mm+4]
        _Ls = Ls[mm:mm+4]
        _betas = betas[mm:mm+4]

        # valore centrale
        minimum = find_F_minimum(_chis, _betas, _chi_errs, _Ls)

        # Faccio bootstrap per stimare l'errore
        with multiprocessing.Pool(processes=None) as p:
            minima = p.starmap(
                find_F_minimum,
                [(np.random.normal(_chis, _chi_errs), _betas, _chi_errs, _Ls) for _ in range(100)]
            )

        cov = np.cov(minima, rowvar=False)
        corr = cov2corr(cov)
        err = np.sqrt(cov.diagonal())

        minima = np.array(minima)
        plt.scatter(*minima.T, s=5)
        plt.errorbar(*minimum, xerr=err[0], yerr=err[1], label=f'{mm}', marker='x')

        print(corr)
        print(f'ν = {minimum[0]} ± {err[0]}\t γ = {minimum[1]} ± {err[1]}')

    plt.plot(nu_v, gamma_v, marker='x', color='red')
    plt.legend()
    plt.xlabel(r'$\nu$')
    plt.ylabel(r'$\gamma$')
    plt.savefig('minimum-bootstrap-4.pdf')
    plt.show()

    # plot del collasso per 6,7,8,9
    for i in range(6, 10):
        x = (1/np.array(betas[i]) - 1/betac)*(Ls[i]**(1/minimum[0]))
        y = chis[i]*(Ls[i]**(-minimum[1]/minimum[0]))
        plt.errorbar(
            x, y, yerr=chi_errs[i]*(Ls[i]**(-minimum[1]/minimum[0])),
            marker='.', markersize='1', linestyle='', label=Ls[i]
        )

    plt.title(r'FSS con minimizzazione')
    plt.xlabel(r'$(T-T_c)L^{1/\nu}$')
    plt.ylabel(r'$\chi L^{-\gamma/\nu}$')
    plt.legend()
    plt.savefig('collasso.pdf')
    plt.show()
