\documentclass[a4paper]{article}
\usepackage{style}
\usepackage{fullpage}
\usepackage[separate-uncertainty=true]{siunitx}
\usepackage{subfig}

\makeatletter
\newcommand\footnoteref[1]{\protected@xdef\@thefnmark{\ref{#1}}\@footnotemark}
\makeatother

\title{
  Metodi numerici per la fisica \\
  Studio del modello di Ising tramite simulazioni Monte Carlo \\
  \textsc{\large Progetto modulo 1}
}

\author{Andrea Ortone \and Marco Venuti}

\begin{document}
\maketitle

\tableofcontents

\section{Richiami teorici}
Lo scopo di questa analisi è quello di studiare la termodinamica del modello di Ising in assenza di campo esterno; l'hamiltoniana è
\[
    H = - J\sum_{\braket{i,j}} \sigma_{i} \sigma_{j}
\]
dove \( \sigma_{i} \in \{-1, 1\} \) sono i valori dello ``spin'' situati ai nodi di un reticolo quadrato \( 2 \)-dimensionale di lato \( L \) e la somma corre sui primi vicini. Nel seguito scegliamo come normalizzazione \( J = 1 \).

È noto che il sistema presenta una transizione di fase continua a una data temperatura \( \beta_{c} > 0 \), in corrispondenza della quale gli andamenti attesi (nel limite termodinamico) per la lunghezza di correlazione \( \xi \) e la suscettività magnetica \( \chi \) sono\footnote{
  \[
      \braket{\sigma_{i}\sigma_{j}} - \braket{\sigma_{i}}\braket{\sigma_{j}} \sim e^{-d(i,j)/\xi}
      \qquad
      m = \frac{1}{L^2} \sum_i \sigma_i
      \qquad
      \chi = \eval{\dpd{m}{h}}_{h=0} = L^{2} \beta (\braket{m^{2}} - \braket{m}^{2})
  \]
}
\[
    \xi \sim \abs{\tau}^{-\nu} \qquad
    \chi \sim \abs{\tau}^{-\gamma}
\]
dove
\[
    \tau = \frac{T-T_{c}}{T_{c}},\quad T = \frac{1}{\beta}
\]
con
\[
    \beta_{c} = \frac{\log(1+\sqrt{2})}{2} \simeq 0.44 \qquad
    \nu = 1 \qquad \gamma = \frac{7}{4}
\]

Nel seguito cercheremo di stimare la temperatura critica e gli esponenti critici qui introdotti tramite delle simulazioni Monte Carlo e facendo uso delle tecniche di finite size scaling. In particolare utilizzeremo che
\begin{itemize}
\item Nel limite termodinamico, alla temperatura critica, \( \xi \) diverge. Per \( L \) finito, ci aspettiamo dunque che, alla ``temperatura critica''\footnote{
      Per \( L \) finito non esiste una transizione di fase, quindi intendiamo che prendiamo, a fissato \( L \), il \( \bar\beta(L) \) per cui \( \chi \) è massima.
    } $\bar T$, la lunghezza di correlazione del sistema finito sia $\xi_L \sim L$ e quindi
    \begin{equation}\label{eq:gamma-su-nu}
        \chi\ped{max}(L) \equiv \chi(\bar T (L)) \sim \xi_L^{\gamma/\nu} \sim L^{\gamma/\nu} \qquad\Rightarrow\qquad
        \log \chi\ped{max}(L) = \frac{\gamma}{\nu} \log{L} + c
    \end{equation}
\item Nel limite termodinamico, usando le definizioni degli esponenti critici si ha
    \begin{equation} \label{eq:chi_asintotica}
        \chi(\xi) \sim \xi^{\gamma/\nu}
    \end{equation}
    A $L$ finito $\chi$ risulta funzione di $L$ e $T$: $\chi = f(L, T)$.
    Usando la definizione degli esponenti critici, possiamo eseguire un cambio di variabile $|\tau| \sim \xi^{-1/\nu} $ e riscrivere $\chi = f_1(L,\xi)$.\\
    Oltre a questo, ci aspettiamo che:
    \begin{itemize}
    \item per valori della temperatura tali per cui $\xi \ll L$, non ci sono effetti di size finito e quindi $\xi = \xi_L $ e l'andamento del limite termodinamico $\chi(\xi) \sim \xi^{\gamma/\nu}$ risulta rispettato (notiamo che tale condizione è possibile solo per sistemi con $L$ molto grande)
    \item per valori della temperatura per cui $\xi \gtrsim L$, avremo effetti di size finito; in questo caso $\xi \neq \xi_L \sim L$ e la suscettività $\chi$ non rispetterà l'andamento del limite termodinamico; in particolare possiamo aspettarci che la deviazione da tale comportamento risulti tanto maggiore quanto è grande il rapporto tra $\xi$ ed $L$.
        Non solo, nel caso di $\xi$ molto grande, ci aspettiamo che il sistema perda memoria della sua struttura microscopica e quindi che l'unica quantità davvero rilevante per determinare la deviazione dal comportamento nel limite termodinamico sia il rapporto $L/\xi$. \\
    \end{itemize}
    Possiamo riassumere quanto sopra esposto affermando che
    \[
        \chi = \xi^{\gamma/\nu} f(L/\xi) \qquad \text{con} \qquad \lim_{x \to \infty} f(x) = 1
    \]
    Per quanto detto al punto precedente, inoltre si ha $ f(x) \sim x^{\gamma/\nu} $ per \( x\to 0 \).
    Mettendo tutto insieme, per temperature prossime a quella critica:
    \[
        \chi \sim L^{\gamma/\nu} f(L/\xi)
    \]
    che ricordando la definizione degli esponenti critici si può riscrivere come:
    \[
        \chi = g\left((T - T_c) L^{1/\nu}\right) L^{\gamma/\nu}
    \]
    ovvero, eseguendo un plot delle curve
    \begin{equation}\label{eq:fss-collasso}
        g(x) = \chi\del{T(x), L} L^{-\gamma/\nu}
    \end{equation}
    dove
    \begin{equation}\label{eq:x_beta}
        x = (T(x) - T_c) L^{1/\nu}
    \end{equation}
    ci si aspetta le curve corrispondenti a diversi $L$ collassino su un'unica curva.
\item Sfruttando quanto detto nel punto precedente si vede inoltre che:
    \begin{equation}\label{eq:beta-c}
        x\ped{max} = \del{\bar T(L) - T_{c}} L^{1/\nu} \quad\Rightarrow\quad
        \bar T(L) = x\ped{max} L^{-1/\nu} + T_{c}
    \end{equation}
    che ci permette di ricavare sia la temperatura critica, estrapolando l'andamento di \( \bar T(L) \) all'aumentare di \( L \), che una stima di \( \nu \).
\end{itemize}

\section{Studio preliminare del sistema}
Abbiamo realizzato delle simulazioni Markov Chain Monte Carlo del sistema\footnote{
  Si veda \texttt{ising.cpp}.
}, al variare di \( L \) e di \( \beta \) con i seguenti parametri:
\begin{center}
    \begin{tabular}{cl}
      $ 15 \leq L \leq 60 $      & \qquad \text{a passi di 5}       \\
      $ 0.38 \leq \beta < 0.46 $ & \qquad  \text{con 70 passi}      \\
      $ \num{2.5e6} L^{2} $      & \qquad \text{Metropolis updates} \\
      $ 20\ L^{2} $              & \qquad \text{updates tra una misura e la successiva}
    \end{tabular}
\end{center}
Per diminuire la durata del transiente di termalizzazione abbiamo deciso di effettuare le simulazioni, separatamente per ciascun $L$, partendo dalla temperatura più alta (inizializzando ciascuno spin in modo casuale con ugual probabilità per i valori $\pm 1$) ed eseguendo in successione le simulazioni per gli altri valori della temperatura in ordine decrescente.

In Figura~\ref{fig:m} è riportato l'andamento del valore assoluto della magnetizzazione in funzione della temperatura per i diversi \( L \). In particolare notiamo che, come atteso:
\begin{itemize}
\item \( \abs{m} \) cresce al diminuire della temperatura, e tende a zero per alta temperatura;
\item la transizione diventa più rapida all'aumentare di \( L \).
\end{itemize}
\begin{figure}[h!]
    \centering
    \includegraphics[scale=0.65]{fig/m-beta}
    \caption{\label{fig:m}valore assoluto della magnetizzazione in funzione della temperatura per i diversi \( L \).}
\end{figure}
Per il calcolo dei valori mostrati abbiamo\footnote{\label{fn:analysis}Si veda \texttt{analysis.py}.}:
\begin{itemize}
\item verificato che il transiente di termalizzazione dell'osservabile fosse trascurabile: in Figura~\ref{fig:termalizzazione} si nota come per l'osservabile \( m \) non ci siano differenze qualitative rilevanti tra le prime e le ultime misure, per due diversi valori di \( \beta \) (corrispondenti alla ``fase ordinata'' e alla ``fase disordinata'');
\item calcolato gli errori utilizzando il metodo \emph{blocking} con blocchi di dimensione 2000. In Figura~\ref{fig:plateau-m} si può vedere come per una data temperatura, si sia raggiunto un \emph{plateau} per ogni valore di \( L \); andamenti analoghi si hanno per tutti i valori della temperatura misurati.
\end{itemize}

\begin{figure}[h!]
    \centering
    \subfloat{\includegraphics[scale=0.5]{fig/begin-bassaT}}
    \subfloat{\includegraphics[scale=0.5]{fig/end-bassaT}}\\
    \subfloat{\includegraphics[scale=0.5]{fig/begin-altaT}}
    \subfloat{\includegraphics[scale=0.5]{fig/end-altaT}}
    \caption{\label{fig:termalizzazione}magnetizzazione calcolata sulle prime e ultime 1000 misure.}
\end{figure}

\begin{figure}[h!]
    \centering
    \includegraphics[scale=0.7]{fig/plateau-m}
    \caption{\label{fig:plateau-m}errore sulla magnetizzazione al variare della dimensione del blocco, per \( \beta = 0.430286 \), per i valori di \( L \) in legenda.}
\end{figure}
\newpage

\section{Analisi}

\subsection{Calcolo di \( \chi \)}
Per ogni \( \beta \) e per ogni \( L \) si è calcolato\footnoteref{fn:analysis} il valore centrale di \( \chi \) come
\[
    \chi = \frac{N \beta}{T\ped{mc} - 1} \sum_{i=1}^{T\ped{mc}} (\abs{m_{i}} - \overline{\abs{m}})^{2}
\]
Gli errori sono stati stimati con il metodo \emph{bootstrap}, eseguendo 1000 ricampionamenti ed estraendo i valori di \( m \) a blocchi di 2000. In Figura~\ref{fig:plateau-chi} si può vedere come in corrispondenza di tale valore si ottiene un \emph{plateau} per ogni valore di \( L \), per un dato valore di \( \beta \), ma il discorso è analogo per tutti i \( \beta \) esplorati.

In Figura~\ref{fig:chi} sono mostrati i valori ottenuti per la suscettività in funzione di \( \beta \), per i diversi \( L \). La linea verticale corrisponde alla temperatura critica del modello nel limite termodinamico.
Notiamo che, come atteso, all'aumentare di \( L \) il picco della suscettività cresce (in accordo con il fatto che nel limite termodinamico \( \chi \) diverge alla temperatura critica) e che il \( \beta \) che corrisponde al massimo si avvicina a \( \beta_{c} \).

\begin{figure}[h!]
    \centering
    \includegraphics[scale=0.8]{fig/chis}
    \caption{\label{fig:chi}suscettività in funzione di \( \beta \), per i diversi valori di \( L \).}
\end{figure}

\begin{figure}[h!]
    \centering
    \includegraphics[scale=0.7]{fig/plateau-chi}
    \caption{\label{fig:plateau-chi}errore sulla suscettività (da moltiplicare per $\beta L^2$) al variare della dimensione del blocco, per \( \beta = 0.425714\).}
\end{figure}

\subsubsection{Verifica della procedura di bootstrap}
Come verifica del procedimento seguito per calcolare gli errori su $\chi$ abbiamo provato a sostituire la procedura di ricampionamento fittizio eseguendo veramente 200 nuove simulazioni indipendenti con gli stessi parametri per la data coppia $(\beta = 0.42, L=30)$.
In corrispondenza di tali valori, in precedenza avevamo ottenuto:
\[
    \chi\ped{bootstrap} = \num{16.549+-0.078}
\]
Calcolando la deviazione standard delle $\chi$ ottenute dalle nuove simulazioni si ottiene invece il valore 0.081, in accordo con quanto ottenuto sopra.

\subsection{Stima della temperatura critica}
Prendendo il valore di \( \beta \) (e di conseguenza \( T \)) tale per cui, per ciascun \( L \), \( \chi \) è massimo, abbiamo eseguito un fit usando come modello la~\eqref{eq:beta-c} ottenendo i seguenti risultati\footnote{Si veda \texttt{betac.py}.}
\begin{gather*}
    \nu = \num{0.82 +- 0.22} \qquad \nu\ped{exp} = 1 \\
    T_{c} = \num{2.281 +- 0.016} \qquad T\ped{c,exp} \simeq 2.269
\end{gather*}
in accordo con quanto atteso; notiamo però che l'errore relativo su \( \nu \) è molto grande, e quindi non possiamo considerare questo un buon metodo per determinare tale esponente. In Figura~\eqref{fig:beta-c} sono riportati i dati e la funzione di best-fit.

\begin{figure}[h!]
    \centering
    \includegraphics[scale=0.7]{fig/betac}
    \caption{\label{fig:beta-c}fit per il calcolo di \( T_{c} \).}
\end{figure}

\subsection{Stima degli esponenti critici}

\subsubsection{Stima di \( \gamma/\nu \)}
Prendendo i valori massimi di \( \chi \) in funzione di \( \beta \), per i vari \( L \), possiamo dare una stima di \( \gamma/\nu \) usando la~\eqref{eq:gamma-su-nu}. Facendo un fit otteniamo\footnote{Si veda \texttt{gamma\_over\_nu.py}.}
\[ \gamma/\nu = \num{1.7723 +- 0.0047} \]
che è distante \( 4.73 \) sigma dal valore atteso \( 7/4 \). Possiamo però provare a ridurre il range del fit, e a variare gli \( L \) considerati.
Nel nostro caso abbiamo considerato sottoinsiemi di 7 valori consecutivi di \( L \) (su un totale di 10 valori) come qui riportato:
\begin{center}
    \begin{tabular}{ccc} \toprule
      range & \( \gamma/\nu \)       & dist/\( \sigma \) \\ \midrule
      0-6   & \num{1.7739 +- 0.0061} & 3.91 \\
      1-7   & \num{1.7763 +- 0.0093} & 2.83 \\
      2-8   & \num{1.777 +- 0.012}   & 2.30 \\
      3-9   & \num{1.765 +- 0.015}   & 1.00 \\ \bottomrule
    \end{tabular}
\end{center}
Come si può notare mano a mano che si escludono gli \( L \) più piccoli il valore ottenuto dal fit si avvicina (in termini dell'errore) al valore atteso. Questo è in accordo con il fatto che la procedura di finite size scaling funziona tanto meglio quanto più gli \( L \) considerati sono grandi.


\subsubsection{Stima degli esponenti critici}
Per determinare i valori degli esponenti critici della transizione abbiamo sfruttato il fatto che le \( \chi(T, L) \) devono ``collassare'' sulla stessa \( g(x) \) una volta operata la trasformazione~\eqref{eq:fss-collasso}. A tal fine, scelte due dimensioni \( L_{i} \) e \( L_{j} \), abbiamo definito la seguente distanza tra le \( g \) ottenute dai corrispondenti dataset sperimentali tramite la~\eqref{eq:fss-collasso}:
\begin{itemize}
\item fissati dei valori tentativi di \( \gamma \) e \( \nu \) per ciascun \( L \) si individua il range di \( x \) (tramite la~\eqref{eq:x_beta}) comune ai due dataset;
\item si scelgono \( n \) punti \( x_{k} \) equispaziati in tale intervallo;
\item si ricavano i corrispondenti \( \beta_{k} \equiv \beta(x_{k}) \), si interpolano linearmente le \( \chi \) ottenute dalle simulazioni e si propagano le incertezze
    \begin{align*}
      \chi(\beta) &= \chi(\beta_{k}) + \frac{\chi(\beta_{k+1})-\chi(\beta_{k})}{\beta_{k+1}-\beta_{k}} (\beta - \beta_{k}) \\
      \Delta \chi(\beta) &= \frac{1}{\beta_{k+1}-\beta_{k}} \sqrt{(\beta_{k+1}-\beta)^{2}\Delta\chi_{k}^{2} + (\beta_{k}-\beta)^{2}\Delta\chi_{k+1}^{2}}
    \end{align*}
\item si calcola, il valore delle \( g_{i}(x_{k}) \) e \( g_{j}(x_{k}) \) per ogni \( k \).
\item si definisce la distanza
    \[
        d_{i,j} = \sum_{k} \frac{\abs{g_{i}(x_{k}) - g_{j}(x_{k})}}{\sqrt{\Delta g_{i}(x_{k})^{2} + \Delta g_{j}(x_{k})^{2}}}
    \]
\end{itemize}
A questo punto si determinano \( \gamma \) e \( \nu \) in modo da minimizzare la funzione
\[ F(\gamma,\nu) = \sum_{i=0}^{n-2} d_{i,i+1} \]
Notiamo che abbiamo considerato solo le distanze tra dataset con $L$ consecutivi e non tutte le possibili coppie perché in quest'ultimo caso avremmo ottenuto (per $L$ molto diversi tra loro) range comuni nella variabile $x$ piuttosto stretti.

Per verificare che questo procedimento fornisca dei risultati ragionevoli, abbiamo effettuato alcune indagini preliminari:
\begin{itemize}
\item Fissato il valore di \( \gamma = 7/4 \), abbiamo plottato il valore della \( F(\nu) \) e viceversa. In entrambi i casi in Figura~\ref{fig:F-noto-gamma-nu} si osserva che \( F \) ha un minimo in prossimità rispettivamente di \( \nu = 1 \) e \( \gamma = 7/4 \).
\item Abbiamo fatto un plot della \( F \) al variare di entrambe le variabili, riportato in Figura~\ref{fig:F-gamma-nu}. Da queste immagini deduciamo che esiste una notevole correlazione tra i valori dei due esponenti critici.
\end{itemize}

\begin{figure}[h!]
    \centering
    \subfloat{\includegraphics[scale=0.5]{fig/F-noto-gamma}}
    \subfloat{\includegraphics[scale=0.5]{fig/F-noto-nu}}
    \caption{\label{fig:F-noto-gamma-nu}grafici della \( F \) fissato alternativamente uno tra \( \gamma \) e \( \nu \) al valore atteso.}
\end{figure}

\begin{figure}[h!]
    \centering
    \subfloat[Le curve rosse e blu corrispondono ai grafici in Figura~\ref{fig:F-noto-gamma-nu}.]{\includegraphics[scale=0.65]{fig/F-gamma-nu}}
    \subfloat[Il punto evidenziato corrisponde ai valori esatti degli esponenti.]{\includegraphics[scale=0.6]{fig/F-gamma-nu-contour}}
    \caption{\label{fig:F-gamma-nu}grafici della \( F(\gamma, \nu) \).}
\end{figure}

A questo punto abbiamo cercato il minimo della funzione \( F \) al variare di entrambi i parametri usando la routine di minimizzazione fornita da \texttt{scipy.optimize.minimize}.
Per fornire una stima dell'errore statistico compiuto abbiamo utilizzato il metodo \emph{bootstrap}: in particolare, dato che conosciamo gli errori statistici sui valori delle $\chi$, abbiamo effettuato $M$ resampling di tali quantità assumendo per queste delle distribuzioni gaussiane indipendenti con deviazione data dall'errore statistico dei dati.
I risultati di tale procedura, visibili anche in Figura~\ref{fig:bootstrap_tot}, sono:
\[
    \nu\ped{min} = \num{0.9355 +- 0.0070} \qquad \gamma\ped{min} = \num{1.6727 +- 0.0079}
\]

\begin{equation}
    \operatorname{Corr}(\nu, \gamma) =
    \begin{pmatrix}
        1 & 0.90 \\
        0.90 & 1
    \end{pmatrix}
\end{equation}
Notiamo che:
\begin{itemize}
\item i risultati ottenuti non risultano compatibili con i valori attesi per gli esponenti critici;
\item come atteso la correlazione dei due esponenti risulta positiva e molto elevata.
\end{itemize}

\begin{figure}[h!]
    \centering
    \includegraphics[scale=0.8]{fig/minimum-bootstrap}
    \caption{\label{fig:bootstrap_tot}risultati della minimizzazione della funzione $F$ e della procedura di resampling con $M=100$.}
\end{figure}
In modo analogo a quanto effettuato nella sezione dedicata alla stima del rapporto $\gamma/\nu$, abbiamo provato a ridurre il range dei valori di \( L \) considerati, ottenendo i seguenti risultati, riassunti anche in Figura~\ref{fig:bootstrap_4}:

\begin{center}
    \begin{tabular}{cccccc} \toprule
      range & \( \nu \)            & \( \gamma \)         & corr & dist/$\sigma_\nu$ & dist/$\sigma_\gamma$ \\ \midrule
      0-3   & \num{0.95 +- 0.010}  & \num{1.683 +- 0.012} & 0.94 & 4.8               & 5.5                  \\
      1-4   & \num{0.93 +- 0.010}  & \num{1.66 +- 0.013}  & 0.85 & 6.9               & 8.1                  \\
      2-5   & \num{0.927 +- 0.016} & \num{1.663 +- 0.014} & 0.79 & 4.5               & 6.0                  \\
      3-6   & \num{0.928 +- 0.019} & \num{1.670 +- 0.016} & 0.70 & 3.8               & 5.0                  \\
      4-7   & \num{0.928 +- 0.031} & \num{1.675 +- 0.022} & 0.76 & 2.3               & 3.4                  \\
      5-8   & \num{0.960 +- 0.041} & \num{1.698 +- 0.022} & 0.66 & 1.0               & 2.3                  \\
      6-9   & \num{0.968 +- 0.063} & \num{1.711 +- 0.028} & 0.61 & 0.51              & 1.4                  \\ \bottomrule
    \end{tabular}
\end{center}
Similmente al caso della sezione predente, possiamo notare che, spostando verso l'alto il range dei valori di \( L \) considerati, si ottiene un maggiore accordo (sia in termini del valore centrale, sia in termini di compatibilità con l'errore fornito dal \emph{bootstrap}) coi valori attesi.
Anche questo risultato è in accordo con il fatto che la procedura di finite size scaling funziona meglio quando le dimensioni del sistema sono più elevate.
Per completezza, in Figura~\ref{fig:collasso} è riportato il grafico del collasso delle \( \chi \) usando gli esponenti ottenuti dalla minimizzazione usando il range \( 6-9 \).

\begin{figure}[h!]
    \centering
    \includegraphics[scale=0.8]{fig/minimum-bootstrap-4}
    \caption{\label{fig:bootstrap_4}risultati della minimizzazione della funzione $F$ e della procedura di resampling con $M=50$, al variare del range di L utilizzato (che si sposta verso l'alto con il crescere del numero in legenda).}
\end{figure}

\begin{figure}[h!]
    \centering
    \includegraphics[scale=0.8]{fig/collasso}
    \caption{\label{fig:collasso}collasso dei plot della suscettività (per i 4 valori di \( L \) più grandi), usando gli esponenti ottenuti dalla minimizzazione.}
\end{figure}


\end{document}
